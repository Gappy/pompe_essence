public class Reservoir {
    private double QUANTITETOTALE = 10000.0; //capacite en L de chaque cuve (SP95,SP98 et gasoil)
    private boolean quantite500 = false; //booleen alerte niveau a 500L pour declencher reapprovisionnement.
    private boolean quantite100 = false; // booleen alerte niveau critique pour blocage de la distribution d'un type carburant donne.
    private double quantiteCarbRestante; //volume restant dans la cuve.

// je sais pas apres j'ai vu que tu voulais faire la distinction entre un niveau maximum et un niveau actuel, oui on va l'utiliser

    //constructeur courant, on accepte la valeur par défaut de QUANTITETOTALE et
    public Reservoir(boolean quantite500, boolean quantite100) {
        this.quantite500 = quantite500;
        this.quantite100 = quantite100;
        this.quantiteCarbRestante = this.QUANTITETOTALE; // init a valeur max
    }

    // contructeur par défaut
    public Reservoir(double QUANTITETOTALE, boolean quantite500, boolean quantite100, double quantiteCarbRestante) {
        this.QUANTITETOTALE = QUANTITETOTALE;
        this.quantite500 = quantite500;
        this.quantite100 = quantite100;
        this.quantiteCarbRestante = quantiteCarbRestante;
    }

    //getters

    public double getQUANTITETOTALE() {
        return QUANTITETOTALE;
    }

    public boolean isQuantite500() {
        return quantite500;
    }

    public void setQuantite500(boolean quantite500) {
        this.quantite500 = quantite500;
    }

    public boolean isQuantite100() {
        return quantite100;
    }

    public void setQuantite100(boolean quantite100) {
        this.quantite100 = quantite100;
    }

    public double getQuantiteCarbRestante() {
        return quantiteCarbRestante;
    }

    public void setQuantiteCarbRestante(double quantiteCarbRestante) {
        this.quantiteCarbRestante = quantiteCarbRestante;
    }

	
/*

	// Vite faut du jus...
	private void remplir(String nomCarburant) {
		Terminal.ecrireStringln("Saute sur ton telephone Marcel, il faut remplir la cuve de " + nomCarburant + "ca urge");
	}
*/

    //fonction de test si vol restant \ temoin en L (100, 500, ...)
    private boolean blocageNiveauInsuff(double quantite, int temoin) {
        double quant = this.getQuantiteCarbRestante() - quantite;
        if (quant <= temoin) {
            return true;
        } else {
            return false;
        }
    }

    protected void soustraireQuantiteCarburant(double quantite) {
        if (blocageNiveauInsuff(quantite, 500) != true) {
            this.setQuantiteCarbRestante(this.getQuantiteCarbRestante() - quantite);
        } else {
            Terminal.ecrireStringln("Quantité de carburant insuffisante");
        }
    }

    //fonction de test si on fait pas deborder cuve...
    private boolean blocageNiveauSup(double quantite) {
        double quant = this.getQuantiteCarbRestante() + quantite;
        if (quant > QUANTITETOTALE) {
            return true;
        } else {
            return false;
        }
    }

    protected void ajouterQuantiteCarburant(double quantite) {
        if (blocageNiveauSup(quantite) != true) {
            this.setQuantiteCarbRestante(this.getQuantiteCarbRestante() + quantite);
        } else {
            Terminal.ecrireStringln("Quantité de carburant maximum atteinte");
        }
    }

/*
    // methodes de verification des volumes de carburant restant dans la cuve du type carburant et quantite demande.TO DO
    private boolean checkQuantiteRestante(String nomCarburant, double quantite){
        boolean servicePossible;
        if((alerteBasNiveau(carburant, quantite) == true) && (!blocageNiveauInsuff(carburant, quantite) == false)) {
            servicePossible=true;
            remplir(carburant);
        }else if (blocageNiveauInsuff(carburant, quantite) == true){
            servicePossible=false;
            remplir(carburant);
        }else (alerteBasNiveau(carburant, quantite) == false){
            servicePossible=true;
        }
        return servicePossible;
    }
	*/
}
