public class Carburant extends Reservoir {

	protected double prixAuLitre;
    protected String NomCarburant;

    public Carburant(boolean quantite500, boolean quantite100, double prixAuLitre, String nomCarburant) {
        super(quantite500, quantite100);
        this.prixAuLitre = prixAuLitre;
        NomCarburant = nomCarburant;
    }

    public double getPrixAuLitre() {
        return prixAuLitre;
    }

    public void setPrixAuLitre(double prixAuLitre) {
        this.prixAuLitre = prixAuLitre;
    }

    public String getNomCarburant() {
        return NomCarburant;
    }

    public void setNomCarburant(String nomCarburant) {
        NomCarburant = nomCarburant;
    }
}
