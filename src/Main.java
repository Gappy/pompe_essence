import java.util.ArrayList;

public class Main {
    // variable d echapement des boucles
    static int ECHAP = 99;
    // collection de Carburant
    private static ArrayList<Carburant> cuves;
    // collection de Borne
    private static ArrayList<Borne> pompes;

    public static void main(String[] args) {
        // instancier une liste de carburants
        cuves = new ArrayList<Carburant>();
        // on ajoute de nouveaux carburants
        cuves.add(new Carburant(false, false, 1.1, "SP95"));
        cuves.add(new Carburant(false, false, 1.2, "SP98"));
        cuves.add(new Carburant(false, false, 1.0, "gazoil"));

        // instancier les 3 bornes dans une liste de bornes
        pompes = new ArrayList<Borne>();
        // on ajoute de nouvelles bornes
        pompes.add(new BorneManuelle(true, true, 1));
        pompes.add(new BorneManuelle(true, true, 2));
        pompes.add(new BorneAuto(true, 3, 100));

        // lancement simulation
        choixSimulation();


        /*
        boolean etatOuverture = false; // pouquoi si on rajoute private il veut obligatoirement du static ????

        while(etatOuverture){
            afficherChoix();
            int choiceGerant = Terminal.lireInt();

            switch(choiceGerant){
                case 0 :
                    getNiveauCuves();
                    break;
                case 1 :
                    blocageBornes();
                    Terminal.ecrireStringln("Bornes 1 et 2 bloqu�es");
                    break;
                case 2 :
                    deblocageBornes();
                    Terminal.ecrireStringln("Bornes 1 et 2 bloqu�es");
                    break;
                case 3 :
                    // TO DO gestion encaissement.
                    break;
                case 4  :
                    // WHAT ELSE
                    break;
                default :
                    Terminal.ecrireStringln("CHOIX NON VALIDE ");
            }
        }
*/
    }

    private static void choixSimulation() {
        int choix = ECHAP;
        do {
            Terminal.ecrireStringln(
                    "Choix simulation: \n" +
                            "Caissier   [1]\n" +
                            "Client     [2]\n" +
                            "Quitter    [0]\n"
            );
            choix = TerminalCharException();
            switch (choix) {
                case 1:
                    simulationCaissier();
                    break;
                case 2:
                    simulationClient();
                    break;
                case 0:
                    choix = 0;
                    break;
                default:
                    choix = ECHAP;
                    break;
            }
        } while (choix != 0);
    }

    static void simulationCaissier() {
        // choix user
        int choix = ECHAP;
        // iterateur boucle
        int i = ECHAP;
        // sous-choix user
        int ch = ECHAP;
        do {
            Terminal.ecrireStringln(
                    "Bienvenue que voulez-vous faire ?\n" +
                            " Afficher le niveau des cuves      [1]\n" +
                            " Bloquer les pompes manuelles      [2]\n" +
                            " Débloquer les pompes manuelles    [3]\n" +
                            " Encaisser un client               [4]\n" +
                            " Remplir cuve                      [5]\n" +
                            " test boutique                     [6]\n" +
                            " Quitter                           [0]\n"
            );
            choix = TerminalCharException();

            switch (choix) {
                case 1:// niveau cuves
                    // pour chaque Carburant dans cuves
                    for (i = 0; i < cuves.size(); i++) {
                        // affichage du nom du carburant et de la quantité restante via les getter
                        Terminal.ecrireStringln(
                                cuves.get(i).getNomCarburant() + "\n" +
                                cuves.get(i).getQuantiteCarbRestante()
                        );
                    }
                    break;
                case 2:// bloquer pompes manuelles
                    // tant que saisie != int
                    do {
                        Terminal.ecrireStringln("Quelle pompe ?");
                        // pour chaque Borne dans la collection pompes
                        for (i = 0; i < pompes.size(); i++) {
                            // on teste le nom de la classe de l'objet appellé, ici on veut une BorneManuelle
                            if (pompes.get(i).getClass().getSimpleName().equals("BorneManuelle")) {
                                // affichage numero de la pompe dans la collection et son etat
                                Terminal.ecrireStringln("Pompe no. " + i + " Etat: " + pompes.get(i).isEtatPompe());
                            }
                        }
                        ch = TerminalCharException();
                    } while (ch == ECHAP);
                    // gestion de sortie de tableau
                    try {
                        // changement de l'etat de la pompe
                        pompes.get(ch).setEtatPompe(false);
                    } catch (IndexOutOfBoundsException ioobe) {
                        // si l'user rentre un chiffre non adéquat on le prévient gentimment
                        Terminal.ecrireStringln("Cette pompe manuelle n'existe pas\n");
                    }
                    break;
                case 3: // debloquer pompes manuelles // meme logique que case 2
                    do {
                        Terminal.ecrireStringln("Quelle pompe ?");
                        for (i = 0; i < pompes.size(); i++) {
                            if (pompes.get(i).getClass().getSimpleName().equals("BorneManuelle")) {
                                Terminal.ecrireStringln("Pompe no. " + i + " Etat: " + pompes.get(i).isEtatPompe());
                            }
                        }
                        ch = TerminalCharException();
                    } while (ch == ECHAP);
                    try {
                        pompes.get(ch).setEtatPompe(true);
                    } catch (IndexOutOfBoundsException ioobe) {
                        Terminal.ecrireStringln("Cette pompe manuelle n'existe pas\n");
                    }
                    break;
                case 4:
                    break;
                case 5:
//todo attention gestion bornes pour paiement
// todo empecher l'utilisation borne si cavalier a false...
                    // choix carburant
                    int chCarbu = ECHAP;
                    do {
                        Terminal.ecrireStringln("Quel carburant ?");
                        for (i = 0; i < cuves.size(); i++) {
                                Terminal.ecrireStringln("Carburant no. " + i + " Volume actuel: " + cuves.get(i).getQuantiteCarbRestante());
                        }
                        chCarbu = TerminalCharException();
                    } while (chCarbu == ECHAP);

                    // choix volume
                    double volume = ECHAP;
                    do {
                        Terminal.ecrireStringln("Quel volume ?");
                        volume = TerminalDoubleException();
                    } while (volume == ECHAP);

                    // on ajoute le carbu
                    Main.getCuves().get(chCarbu).ajouterQuantiteCarburant(volume);

                    // verification
                    for (i = 0; i < cuves.size(); i++) {
                        // affichage du nom du carburant et de la quantité restante via les getter
                        Terminal.ecrireStringln(
                                cuves.get(i).getNomCarburant() + "\n" +
                                        cuves.get(i).getQuantiteCarbRestante()
                        );
                    }
                    break;
                case 6:
                    Boutique BOU = new Boutique();
                    BOU.afficherChoix();
                    break;
                case 0:
                    choix = 0;
                    break;
                default:
                    choix = ECHAP;
                    break;
            }
        } while (choix != 0);
    }

    static void simulationClient() {
        // choix user
        int choix = ECHAP;
        // iterateur boucle
        int i = ECHAP;
        // sous-choix user
        int ch = ECHAP;

        // choix simulation client
        do {
            Terminal.ecrireStringln(
                    "Bienvenue que voulez-vous faire ?\n" +
                            " Retirer du carburant connard      [1]\n" +
                            " Quitter                           [0]"
            );
            choix = TerminalCharException();

            switch (choix) {
                case 1:
                    // choix Borne (pompe)
                    int chPompe;
                    do {
                        Terminal.ecrireStringln("Quelle pompe souhaitez-vous ?\n");
                        // pour chaque Borne dans la collection pompes
                        for (i = 0; i < pompes.size(); i++) {
                            Terminal.ecrireStringln("Pompe no. " + i + " Type: " + pompes.get(i).getClass().getName());
                        }
                        chPompe = TerminalCharException();
                    } while (chPompe == ECHAP);

                    // simu affichage client
                    pompes.get(chPompe).affichageConsoleClient("", 0.0, 0.0);

                    // choix carburant
                    int chCarbu;
                    do {
                        Terminal.ecrireStringln("Quelle carburant souhaitez-vous ?\n");
                        // pour chaque Carburant dans collection cuves
                        for (i = 0; i < cuves.size(); i++) {
                            Terminal.ecrireStringln(cuves.get(i).getNomCarburant() + " : " + cuves.get(i).getPrixAuLitre() + "\t\t[" + i + "]");
                        }
                        chCarbu = TerminalCharException();
                    } while (chCarbu == ECHAP);

                    // simu affichage client
                    pompes.get(chPompe).affichageConsoleClient(cuves.get(chCarbu).getNomCarburant(), cuves.get(chCarbu).getPrixAuLitre(), 0.0);

                    // choix volume
                    double chVolume;
                    do {
                        Terminal.ecrireStringln("Quelle quantité souhaitez-vous ?\n");
                        chVolume = TerminalDoubleException();
                    } while (chVolume == ECHAP);

                    // debit de la cuve
                    cuves.get(chCarbu).soustraireQuantiteCarburant(chVolume);

                    // calcul du prix: utilisation index choix carburant pour cuves, recuperation prix * volume
                    Terminal.ecrireStringln("Ca vous fait: " + cuves.get(chCarbu).getPrixAuLitre() * chVolume);

                    // simulation affichage console Borne pour client
                    pompes.get(chPompe).affichageConsoleClient(
                            cuves.get(chCarbu).getNomCarburant(),   // nom carburant
                            cuves.get(chCarbu).getPrixAuLitre(),    // prix / L
                            chVolume                                // volume
                    );

                break;
                case 0:
                    choix = 0;
                    break;
                default:
                    choix = ECHAP;
                    break;
            }
        } while (choix != 0);
    }


    static int TerminalCharException() {
        int val = ECHAP;
        try {
            val = Terminal.lireInt();
            return val;
        } catch (Exception e) {
        } finally {
            return val;
        }
    }

    static double TerminalDoubleException() {
        double val = (double) ECHAP;
        try {
            val = Terminal.lireDouble();
            return val;
        } catch (Exception e) {
        } finally {
            return val;
        }
    }

    public static int getECHAP() {
        return ECHAP;
    }

    public static ArrayList<Carburant> getCuves() {
        return cuves;
    }

    public static ArrayList<Borne> getPompes() {
        return pompes;
    }

}

