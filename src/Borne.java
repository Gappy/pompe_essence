public class Borne {
    protected boolean etatPompe;
    protected int idBorne;

    public Borne(boolean etatPompe, int idBorne) {
        this.etatPompe = etatPompe;
        this.idBorne = idBorne;
    }
/*
    public double getPrixAuLitre() {
        return prixAuLitre;
    }

    public void setPrixAuLitre(double prixAuLitre) {
        this.prixAuLitre = prixAuLitre;
    }
*/

    public boolean isEtatPompe() {
        return etatPompe;
    }

    public void setEtatPompe(boolean etatPompe) {
        this.etatPompe = etatPompe;
    }

    public int getIdBorne() {
        return idBorne;
    }

    public void setIdBorne(int idBorne) {
        this.idBorne = idBorne;
    }

    // methode qui simule retrait de carburant par un client
    private void retirerCarburant (String nomCarburant){
        //choix quantité et tests associés
        //appell soustraire quantité de reservoir
        //appel calculer prix etc

    }

    public void affichageConsoleClient(String carburant, Double prixAuLitre, Double volume){
        String carb = "";
        Double pL = 0.0;
        Double vol = 0.0;
        if(carburant.isEmpty() == false){
            carb = carburant;
        }
        if(prixAuLitre != 0.0){
            pL = prixAuLitre;
        }
        if(volume != 0.0){
            vol = volume;
        }

        Terminal.ecrireStringln(
          "Carburant:\t\t" + carb + "\n" +
          "Prix/L:\t\t" + pL + " €/L\n" +
          "Volume:\t\t" + vol + " L\n" +
          "Prix total:\t\t" + pL * vol + " €\n"
        );
    }


    // methode pour effacer affichage pour client suivant
    protected void reinitAffichage() {
        Terminal.ecrireStringln(
            "Carburant:\t\t" + "\n" +
            "Prix/L:\t\t" + " €/L\n" +
            "Volume:\t\t" + " L\n" +
            "Prix total:\t\t" + " €\n"
        );
    }
}

