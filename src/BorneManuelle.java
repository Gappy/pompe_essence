public class BorneManuelle extends Borne{
    protected double VOLUME_MAX = 100.0;
    protected int DELAI_PAIEMENT = 15;
    protected boolean etatPaiement;

    //constructeur par défaut
    public BorneManuelle(boolean etatPaiement, boolean etatPompe, int idBorne, double VOLUME_MAX, int DELAI_PAIEMENT) {
        super(etatPompe, idBorne);
        this.VOLUME_MAX = VOLUME_MAX;
        this.DELAI_PAIEMENT = DELAI_PAIEMENT;
        this.etatPaiement = etatPaiement;
    }

    //constructeur courant, on accepte la valeur par défaut de VOLUME_MAX et DELAI_PAIEMENT
    public BorneManuelle(boolean etatPaiement, boolean etatPompe, int idBorne) {
        super(etatPompe, idBorne);
        this.etatPaiement = etatPaiement;
    }

    public boolean isEtatPaiement() {
        return etatPaiement;
    }

    public void setEtatPaiement(boolean etatPaiement) {
        this.etatPaiement = etatPaiement;
    }

    public double getVOLUME_MAX() {
        return VOLUME_MAX;
    }

    public void setVOLUME_MAX(double VOLUME_MAX) {
        this.VOLUME_MAX = VOLUME_MAX;
    }

    public int getDELAI_PAIEMENT() {
        return DELAI_PAIEMENT;
    }

    public void setDELAI_PAIEMENT(int DELAI_PAIEMENT) {
        this.DELAI_PAIEMENT = DELAI_PAIEMENT;
    }

    private double calculPrixTotal(double volume, double prixAuLitre, String nomCarburant){
        double prixAfacturer = 0.0;
        return prixAfacturer;
    }




    // remet les compteurs à 0
    private void resetBorne(){

    }

    //fonction avant modifs TO DO FIX
    private double envoiDetailBoutique(double montantTotalServi){
        double montantTotal = 50; // TO DO appel de methode calcul du total en fonction volume pris.
        return montantTotal;
    }
}
