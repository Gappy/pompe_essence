package pompe_essence;

//TO DO arrayliste de reservoir � creer dans le main.

public class Reservoir {
	public static final double QUANTITETOTALE	 =10000.0; //capacite en L de chaque cuve (SP95,SP98 et gasoil)
	private boolean quantite500 = false; //booleen alerte niveau a 500L pour declencher reapprovisionnement.
	private boolean quantite100 = false; // booleen alerte niveau critique pour blocage de la distribution d'un type carburant donne.
	private double quantiteCarbRestante =10000.0; //volume restant dans la cuve.	
	private typeCarburant typeCarburant;
	
	//constructeur
	protected Reservoir(typeCarburant typeCarburant){ // interet du protected ?
		this.typeCarburant = typeCarburant;
		this.quantiteCarbRestante = QUANTITETOTALE;
	}
	
	//getters
	public double getQuantiteCarbRestant(typeCarburant typeCarburant) {
		return this.quantiteCarbRestante;
	}
	
	public typeCarburant getTypeCarburant(){
		return this.typeCarburant;
	}
	
	// methodes de verification des volumes de carburant restant dans la cuve du type carburant et quantit� demand�.
	private boolean checkQuantiteRestante(typeCarburant typeCarburant, double quantite){
		boolean servicePossible;
		if((checkQuantite500(typeCarburant, quantite) == true) && (!checkQuantite100(typeCarburant, quantite) == false)) { // WTF OnlySynchronized ?
			servicePossible=true;
			ravitaillement(typeCarburant);
		}else if (checkQuantite100(typeCarburant, quantite) == true){
			servicePossible=false;
			ravitaillement(typeCarburant);
		}else (checkQuantite500(typeCarburant, quantite) == false){
			servicePossible=true;
		}
		return servicePossible;
	}
	
	// Vite faut du jus...
	private void ravitaillement(pompe_essence.typeCarburant typeCarburant) {
		Terminal.ecrireStringln("Saute sur ton t�l�phone Marcel, il faut remplir la cuve de " + typeCarburant + "�a urge");
	}
	
	//fonction de test si vol restant <500L
	private boolean checkQuantite500(typeCarburant typeCarburant, double quantite){
		double quant = this.getQuantiteCarbRestant(typeCarburant) - quantite;
		if(quant <= 500){
			return true;
		}else{
		return false;	
		}	
	}
	
	//fonction de test si vol restant <100L
	private boolean checkQuantite100(typeCarburant typeCarburant, double quantite){
		double quant = this.getQuantiteCarbRestant(typeCarburant) - quantite;
		if(quant <= 100){
			return true;
		}else{
		return false;	
		}
	}
	
	
	
}
